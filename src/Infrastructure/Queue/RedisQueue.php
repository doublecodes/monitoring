<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Queue;

use Predis\ClientInterface;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;
use Talentry\Monitoring\Infrastructure\Serializer\SerializerFactory;

class RedisQueue implements Queue
{
    private const QUEUE_NAME = 'MONITORING_REDIS_QUEUE';

    private Serializer $serializer;

    public function __construct(
        private ClientInterface $redis,
        ?Serializer $serializer = null
    ) {
        $this->serializer = $serializer ?? (new SerializerFactory())->generate();
    }

    public function push(Metric $metric): void
    {
        $this->redis->rpush(
            self::QUEUE_NAME,
            [$this->serializer->serialize($metric)]
        );
    }

    public function pop(): ?Metric
    {
        $payload = $this->redis->lpop(self::QUEUE_NAME);

        if (empty($payload)) {
            return null;
        }

        return $this->serializer->unserialize($payload);
    }
}
