<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Queue;

use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Infrastructure\Monitor\QueuedMonitor;

class QueuedMetricsProcessor
{
    public function __construct(
        private Monitor $monitor
    ) {
    }

    public function run(): int
    {
        if ($this->monitor instanceof QueuedMonitor) {
            return $this->monitor->processQueue();
        }

        return 0;
    }
}
