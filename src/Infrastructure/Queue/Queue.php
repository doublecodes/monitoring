<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Queue;

use Talentry\Monitoring\Domain\Metric\Model\Metric;

interface Queue
{
    public function push(Metric $metric): void;
    public function pop(): ?Metric;
}
