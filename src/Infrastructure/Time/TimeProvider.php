<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Time;

class TimeProvider
{
    public function getCurrentTimeStampInMilliseconds(): int
    {
        return (int) (microtime(true) * 1000);
    }
}
