<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Metric;

use Talentry\Monitoring\Domain\Metric\MetricStore;

class VoidMetricStore implements MetricStore
{
    public function increment(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
    }

    public function decrement(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
    }

    public function gauge(
        string $metric,
        float $value,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
    }

    public function timing(
        string $metric,
        float $time,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
    }
}
