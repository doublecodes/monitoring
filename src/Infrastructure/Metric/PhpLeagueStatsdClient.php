<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Metric;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use League\StatsD\Client;
use Throwable;

class PhpLeagueStatsdClient implements MetricStore
{
    private Client $client;

    public function __construct(
        string $statsdHost,
        int $statsdPort,
        private LoggerInterface $logger = new NullLogger(),
    ) {
        $this->client = new Client();
        $this->client->configure([
            'host' => $statsdHost,
            'port' => $statsdPort,
        ]);
    }

    public function increment(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        try {
            $this->client->increment($this->applyPrefix($metric, $namespace), 1, $sampleRate, $tags ?? []);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function decrement(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        try {
            $this->client->decrement($this->applyPrefix($metric, $namespace), 1, $sampleRate, $tags ?? []);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function gauge(
        string $metric,
        float $value,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        try {
            $this->client->gauge($this->applyPrefix($metric, $namespace), $value, $tags ?? []);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function timing(
        string $metric,
        float $time,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        try {
            $this->client->timing($this->applyPrefix($metric, $namespace), $time, $tags ?? []);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }
    }

    private function applyPrefix(string $metric, ?string $namespace = null): string
    {
        if ($namespace === null) {
            return $metric;
        }

        return sprintf('%s.%s', $namespace, $metric);
    }
}
