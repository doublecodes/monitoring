<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Metric;

use Psr\Log\LoggerInterface;
use Talentry\ErrorHandling\Enum\Severity;
use Talentry\ErrorHandling\Error\Warning;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use Datadogstatsd;

/**
 * Datadog uses an extended statsd protocol, so we use their SDK to implement the client
 */
class DatadogStatsdClient implements MetricStore
{
    public function __construct(
        private string $host,
        private int $port,
        private ErrorHandler $errorHandler,
        private LoggerInterface $logger,
    ) {
    }

    public function increment(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        $this->proxy('increment', [$this->applyPrefix($metric, $namespace), $sampleRate, $tags]);
    }

    public function decrement(
        string $metric,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        $this->proxy('decrement', [$this->applyPrefix($metric, $namespace), $sampleRate, $tags]);
    }

    public function gauge(
        string $metric,
        float $value,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        $this->proxy('gauge', [$this->applyPrefix($metric, $namespace), $value, $sampleRate, $tags]);
    }

    public function timing(
        string $metric,
        float $time,
        float $sampleRate = 1.0,
        array $tags = null,
        ?string $namespace = null
    ): void {
        $this->proxy('timing', [$this->applyPrefix($metric, $namespace), $time, $sampleRate, $tags]);
    }

    private function proxy(string $method, array $args): void
    {
        try {
            $this->errorHandler->startHandling(new Severity(Severity::WARNING));
            Datadogstatsd::configure(null, null, null, null, $this->host, $this->port);
            call_user_func_array([Datadogstatsd::class, $method], $args);
        } catch (Warning $warning) {
            $this->logger->error($warning->getMessage(), ['trace' => $warning->getTraceAsString()]);
        } finally {
            $this->errorHandler->stopHandling();
        }
    }

    private function applyPrefix(string $metric, ?string $namespace = null): string
    {
        if ($namespace === null) {
            return $metric;
        }

        return sprintf('%s.%s', $namespace, $metric);
    }
}
