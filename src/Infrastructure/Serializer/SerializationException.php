<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer;

use RuntimeException;

class SerializationException extends RuntimeException
{
}
