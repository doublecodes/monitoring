<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer;

use Talentry\Monitoring\Domain\Metric\Model\Metric;

interface Serializer
{
    public function serialize(Metric $metric): string;
    public function unserialize(string $serialized): Metric;
    public function supportsSerialization(Metric $metric): bool;
    public function supportsUnserialization(string $serialized): bool;
}
