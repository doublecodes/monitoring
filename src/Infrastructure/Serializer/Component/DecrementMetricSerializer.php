<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer\Component;

use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Infrastructure\Serializer\SerializationException;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;

class DecrementMetricSerializer implements Serializer
{
    public function serialize(Metric $metric): string
    {
        if (!$this->supportsSerialization($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        /** @var $metric DecrementMetric */
        return json_encode([
            '_type' => DecrementMetric::class,
            'name' => $metric->getName(),
        ]);
    }

    public function unserialize(string $serialized): Metric
    {
        if (!$this->supportsUnserialization($serialized)) {
            throw new SerializationException();
        }

        $array = json_decode($serialized, true);

        return Metrics::decrementMetric($array['name']);
    }

    public function supportsSerialization(Metric $metric): bool
    {
        return $metric instanceof DecrementMetric;
    }

    public function supportsUnserialization(string $serialized): bool
    {
        $array = json_decode($serialized, true);
        $requiredKeys = [
            '_type',
            'name',
        ];

        foreach ($requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $array)) {
                return false;
            }
        }

        if ($array['_type'] !== DecrementMetric::class) {
            return false;
        }

        return true;
    }
}
