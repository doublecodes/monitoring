<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer\Component;

use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Infrastructure\Serializer\SerializationException;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;

class IncrementMetricSerializer implements Serializer
{
    public function serialize(Metric $metric): string
    {
        if (!$this->supportsSerialization($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        /** @var $metric IncrementMetric */
        return json_encode([
            '_type' => IncrementMetric::class,
            'name' => $metric->getName(),
        ]);
    }

    public function unserialize(string $serialized): Metric
    {
        if (!$this->supportsUnserialization($serialized)) {
            throw new SerializationException();
        }

        $array = json_decode($serialized, true);

        return Metrics::incrementMetric($array['name']);
    }

    public function supportsSerialization(Metric $metric): bool
    {
        return $metric instanceof IncrementMetric;
    }

    public function supportsUnserialization(string $serialized): bool
    {
        $array = json_decode($serialized, true);
        $requiredKeys = [
            '_type',
            'name',
        ];

        foreach ($requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $array)) {
                return false;
            }
        }

        if ($array['_type'] !== IncrementMetric::class) {
            return false;
        }

        return true;
    }
}
