<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer\Component;

use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Infrastructure\Serializer\SerializationException;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;

class GaugeMetricSerializer implements Serializer
{
    public function serialize(Metric $metric): string
    {
        if (!$this->supportsSerialization($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        /** @var $metric GaugeMetric */
        return json_encode([
            '_type' => GaugeMetric::class,
            'name' => $metric->getName(),
            'value' => $metric->getValue(),
        ]);
    }

    public function unserialize(string $serialized): Metric
    {
        if (!$this->supportsUnserialization($serialized)) {
            throw new SerializationException();
        }

        $array = json_decode($serialized, true);

        return Metrics::gaugeMetric($array['name'], $array['value']);
    }

    public function supportsSerialization(Metric $metric): bool
    {
        return $metric instanceof GaugeMetric;
    }

    public function supportsUnserialization(string $serialized): bool
    {
        $array = json_decode($serialized, true);

        $requiredKeys = [
            '_type',
            'name',
            'value',
        ];

        foreach ($requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $array)) {
                return false;
            }
        }

        if ($array['_type'] !== GaugeMetric::class) {
            return false;
        }

        return true;
    }
}
