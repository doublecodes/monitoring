<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer\Component;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Infrastructure\Serializer\SerializationException;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;

class TimingMetricSerializer implements Serializer
{
    public function serialize(Metric $metric): string
    {
        if (!$this->supportsSerialization($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        /** @var $metric TimingMetric */
        return json_encode([
            '_type' => TimingMetric::class,
            'name' => $metric->getName(),
            'timing' => $metric->getTiming(),
        ]);
    }

    public function unserialize(string $serialized): Metric
    {
        $array = json_decode($serialized, true);
        if (!$this->supportsUnserialization($serialized)) {
            throw new SerializationException();
        }

        return Metrics::timingMetric($array['name'], $array['timing']);
    }

    public function supportsSerialization(Metric $metric): bool
    {
        return $metric instanceof TimingMetric;
    }

    public function supportsUnserialization(string $serialized): bool
    {
        $array = json_decode($serialized, true);
        $requiredKeys = [
            '_type',
            'name',
            'timing',
        ];

        foreach ($requiredKeys as $requiredKey) {
            if (!array_key_exists($requiredKey, $array)) {
                return false;
            }
        }

        if ($array['_type'] !== TimingMetric::class) {
            return false;
        }

        return true;
    }
}
