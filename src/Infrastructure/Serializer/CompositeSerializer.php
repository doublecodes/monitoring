<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Serializer;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;

class CompositeSerializer implements Serializer
{
    /**
     * @var Serializer[]
     */
    private array $serializers;

    public function serialize(Metric $metric): string
    {
        if (!$this->supportsSerialization($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        foreach ($this->serializers as $serializer) {
            if ($serializer->supportsSerialization($metric)) {
                return $serializer->serialize($metric);
            }
        }
    }

    public function unserialize(string $serialized): Metric
    {
        if (!$this->supportsUnserialization($serialized)) {
            throw new SerializationException();
        }

        foreach ($this->serializers as $serializer) {
            if ($serializer->supportsUnserialization($serialized)) {
                return $serializer->unserialize($serialized);
            }
        }
    }

    public function supportsSerialization(Metric $metric): bool
    {
        foreach ($this->serializers as $serializer) {
            if ($serializer->supportsSerialization($metric)) {
                return true;
            }
        }

        return false;
    }

    public function supportsUnserialization(string $serialized): bool
    {
        foreach ($this->serializers as $serializer) {
            if ($serializer->supportsUnserialization($serialized)) {
                return true;
            }
        }

        return false;
    }

    public function addSerializer(Serializer $serializer): self
    {
        $this->serializers[] = $serializer;

        return $this;
    }
}
