<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Infrastructure\Queue\Queue;
use Talentry\Monitoring\Domain\Monitor\Monitor;

class QueuedMonitor implements Monitor
{
    public function __construct(
        private Monitor $delegate,
        private Queue $queue
    ) {
    }

    public function push(Metric $metric): void
    {
        if (!$this->supports($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        $this->queue->push($metric);
    }

    public function supports(Metric $metric): bool
    {
        return $this->delegate->supports($metric);
    }

    public function processQueue(): int
    {
        $count = 0;
        while ($metric = $this->queue->pop()) {
            $this->delegate->push($metric);
            $count++;
        }

        return $count;
    }
}
