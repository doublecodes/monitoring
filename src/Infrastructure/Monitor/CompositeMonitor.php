<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Domain\Monitor\Monitor;

class CompositeMonitor implements Monitor
{
    /**
     * @var Monitor[]
     */
    private array $monitors;

    public function push(Metric $metric): void
    {
        if (!$this->supports($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        foreach ($this->monitors as $monitor) {
            if ($monitor->supports($metric)) {
                $monitor->push($metric);
                break;
            }
        }
    }

    public function supports(Metric $metric): bool
    {
        foreach ($this->monitors as $monitor) {
            if ($monitor->supports($metric)) {
                return true;
            }
        }

        return false;
    }

    public function addMonitor(Monitor $monitor): self
    {
        $this->monitors[] = $monitor;

        return $this;
    }
}
