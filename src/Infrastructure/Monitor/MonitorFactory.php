<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Monitor;

use Talentry\Monitoring\Domain\Metric\MetricStore;
use Talentry\Monitoring\Domain\Monitor\DecrementMonitor;
use Talentry\Monitoring\Domain\Monitor\GaugeMonitor;
use Talentry\Monitoring\Domain\Monitor\IncrementMonitor;
use Talentry\Monitoring\Domain\Monitor\TimingMonitor;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Domain\Monitor\VoidMonitor;
use Talentry\Monitoring\Infrastructure\Queue\Queue;

class MonitorFactory
{
    /**
     * @param bool $enabled - Setting to false will bypass the monitoring library, regardless of the other settings
     */
    public function __construct(
        private ?MetricStore $metricStore = null,
        private ?Queue $queue = null,
        private bool $enabled = true,
        private ?string $namespace = null
    ) {
    }

    public function generate(): Monitor
    {
        $monitor = new VoidMonitor();
        if (!$this->enabled) {
            return $monitor;
        }

        if ($this->metricStore) {
            $monitor = new CompositeMonitor();
            $monitor
                ->addMonitor(new IncrementMonitor($this->metricStore, $this->namespace))
                ->addMonitor(new DecrementMonitor($this->metricStore, $this->namespace))
                ->addMonitor(new GaugeMonitor($this->metricStore, $this->namespace))
                ->addMonitor(new TimingMonitor($this->metricStore, $this->namespace))
            ;
        }

        if ($this->queue) {
            $monitor = new QueuedMonitor($monitor, $this->queue);
        }

        return $monitor;
    }
}
