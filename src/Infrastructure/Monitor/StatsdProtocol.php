<?php
// phpcs:ignoreFile TODO remove when PHPCS adds support for enums
declare(strict_types=1);

namespace Talentry\Monitoring\Infrastructure\Monitor;

enum StatsdProtocol: string
{
    case STANDARD = 'standard';
    case DATADOG = 'datadog';
    case AWS_EMF = 'aws_emf'; //not really a statsd protocol, but it simplifies to model it that way

    public function isDatadog(): bool
    {
        return $this === self::DATADOG;
    }

    public function isStandard(): bool
    {
        return $this === self::STANDARD;
    }

    public function isAwsEmf(): bool
    {
        return $this === self::AWS_EMF;
    }

    public static function default(): self
    {
        return self::STANDARD;
    }
}
