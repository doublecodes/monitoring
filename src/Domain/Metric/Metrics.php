<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric;

use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;

/**
 * This is a utility class to simplify creation of metrics.
 * The recommended way of creating metrics is by creating a class that implements one of the Metric interfaces:
 * DecrementMetric, IncrementMetric, GaugeMetric or TimingMetric.
 * However, for some purposes (eg testing) it may be useful to create a metric on the fly without creating a class.
 */
class Metrics
{
    public static function incrementMetric(string $name): IncrementMetric
    {
        return new class ($name) implements IncrementMetric {
            public function __construct(private string $name)
            {
            }

            public function getName(): string
            {
                return $this->name;
            }
        };
    }

    public static function decrementMetric(string $name): DecrementMetric
    {
        return new class ($name) implements DecrementMetric {
            public function __construct(private string $name)
            {
            }

            public function getName(): string
            {
                return $this->name;
            }
        };
    }

    public static function gaugeMetric(string $name, float $value): GaugeMetric
    {
        return new class ($name, $value) implements GaugeMetric {
            public function __construct(private string $name, private float $value)
            {
            }

            public function getName(): string
            {
                return $this->name;
            }

            public function getValue(): float
            {
                return $this->value;
            }
        };
    }

    public static function timingMetric(string $name, float $timing): TimingMetric
    {
        return new class ($name, $timing) implements TimingMetric {
            public function __construct(private string $name, private float $timing)
            {
            }

            public function getName(): string
            {
                return $this->name;
            }

            public function getTiming(): float
            {
                return $this->timing;
            }
        };
    }
}
