<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric\Model;

interface TimingMetric extends Metric
{
    /**
     * Timing should be provided in milliseconds.
     */
    public function getTiming(): float;
}
