<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric\Model;

interface IncrementMetric extends Metric
{
}
