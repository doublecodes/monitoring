<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric\Model;

/**
 * Base Metric interface. DO NOT IMPLEMENT this interface directly. Instead, implement one of the sub-interfaces:
 * IncrementMetric, DecrementMetric, GaugeMetric or TimingMetric
 */
interface Metric
{
    /**
     * Metric name. Should be lower-case and should use dot-separated notation for creating namespaces,
     * eg: system.load.memory and system.load.disk
     */
    public function getName(): string;
}
