<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Metric\Model;

interface GaugeMetric extends Metric
{
    public function getValue(): float;
}
