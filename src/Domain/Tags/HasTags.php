<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Tags;

interface HasTags
{
    public function getTags(): array;
}
