<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Tags\HasTags;
use Talentry\Monitoring\Domain\Metric\MetricStore;

abstract class MetricMonitor implements Monitor
{
    protected const DEFAULT_SAMPLE_RATE = 1.0;

    public function __construct(
        private MetricStore $metricStore,
        protected ?string $namespace = null
    ) {
    }

    public function getMetricStore(): MetricStore
    {
        return $this->metricStore;
    }

    public function getTags(Metric $metric): array
    {
        $tags = [];
        if ($metric instanceof HasTags) {
            $tags = $metric->getTags();
        }

        return $tags;
    }
}
