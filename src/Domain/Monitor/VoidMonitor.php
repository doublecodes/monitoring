<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;

class VoidMonitor implements Monitor
{
    public function push(Metric $metric): void
    {
    }

    public function supports(Metric $metric): bool
    {
        return true;
    }
}
