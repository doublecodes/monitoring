<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;

class TimingMonitor extends MetricMonitor
{
    public function push(Metric $metric): void
    {
        /** @var TimingMetric $metric */
        if (!$this->supports($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        $this->getMetricStore()->timing(
            $metric->getName(),
            $metric->getTiming(),
            self::DEFAULT_SAMPLE_RATE,
            $this->getTags($metric),
            $this->namespace
        );
    }

    public function supports(Metric $metric): bool
    {
        return $metric instanceof TimingMetric;
    }
}
