<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;

class GaugeMonitor extends MetricMonitor
{
    public function push(Metric $metric): void
    {
        /** @var GaugeMetric $metric */
        if (!$this->supports($metric)) {
            throw new UnsupportedMetricException($metric);
        }

        $this->getMetricStore()->gauge(
            $metric->getName(),
            $metric->getValue(),
            self::DEFAULT_SAMPLE_RATE,
            $this->getTags($metric),
            $this->namespace
        );
    }

    public function supports(Metric $metric): bool
    {
        return $metric instanceof GaugeMetric;
    }
}
