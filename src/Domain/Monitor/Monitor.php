<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Domain\Monitor;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;

interface Monitor
{
    /**
     * This is the only public method that should be called from the outside.
     *
     * @api
     * @throws UnsupportedMetricException
     */
    public function push(Metric $metric): void;

    /**
     * Checks whether or not this Monitor supports the provided Metric.
     */
    public function supports(Metric $metric): bool;
}
