<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Presentation\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\Monitoring\Infrastructure\Queue\QueuedMetricsProcessor;

class PushQueuedMetricsCommand extends Command
{
    public const COMMAND_NAME = 'ty:monitoring:push-queued-metrics';

    public function __construct(
        private QueuedMetricsProcessor $queuedMetricsProcessor,
        private LoggerInterface $logger
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        parent::configure();

        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Gets queued metrics and pushes them to metric store')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $processed = $this->queuedMetricsProcessor->run();

        $this->logger->debug(sprintf('Processed %s metrics', $processed));

        return 0;
    }
}
