<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Presentation\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Monitor\Monitor;

class PushTestMetricCommand extends Command
{
    public const COMMAND_NAME = 'ty:monitoring:push-test-metric';

    public function __construct(
        private Monitor $monitor
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Pushes a test metric to the monitoring service')
            ->addOption('name', null, InputOption::VALUE_REQUIRED, '', 'test.metric')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $metric = Metrics::incrementMetric($input->getOption('name'));

        $this->monitor->push($metric);

        return 0;
    }
}
