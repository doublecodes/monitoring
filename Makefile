PROJECT=monitoring

up:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) up -d
down:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) down
test:
	docker-compose -f infrastructure/dev/docker-compose.yml -p $(PROJECT) exec php-cli vendor/bin/phpunit
deps:
	docker run -v $(shell pwd):/app --rm -t composer install --ignore-platform-reqs
