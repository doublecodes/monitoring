<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests;

use PHPUnit\Framework\TestCase;
use Talentry\ErrorHandling\ErrorHandler;
use Talentry\ErrorHandling\Factory\ErrorHandlerFactory;

abstract class StatsdClientTest extends TestCase
{
    private $socket;
    protected string $host;
    protected int $port;
    protected string $metricName = 'test';
    protected array $metricTags = ['foo' => 'bar', 'bar' => 'baz'];
    protected float $metricValue = 42.0;
    protected string $namespace = 'ns';
    protected float $sampleRate = 1.0;
    protected ErrorHandler $errorHandler;

    protected function setUp(): void
    {
        parent::setUp();

        $this->host = '127.0.0.1';
        $this->port = 12_345;
        $this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        socket_bind($this->socket, $this->host, $this->port);
        socket_set_option($this->socket, SOL_SOCKET, SO_RCVTIMEO, ['sec' => 1, 'usec' => 0]);
        $this->errorHandler = (new ErrorHandlerFactory())->generate();
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        socket_close($this->socket);
    }

    protected function readSocket(): string
    {
        $name = null;
        $buffer = '';

        socket_recvfrom($this->socket, $buffer, 1024, 0, $name, $this->port);

        return $buffer;
    }

    protected function assertIncrementMetricWasPushed(): void
    {
        self::assertSame(sprintf('%s:1|c', $this->metricName), $this->readSocket());
    }

    protected function assertIncrementMetricWithNamespaceWasPushed(): void
    {
        self::assertSame(sprintf('%s.%s:1|c', $this->namespace, $this->metricName), $this->readSocket());
    }

    protected function assertIncrementMetricWithTagsWasPushed(): void
    {
        self::assertSame($this->withTags(sprintf('%s:1|c', $this->metricName), $this->metricTags), $this->readSocket());
    }

    protected function assertDecrementMetricWasPushed(): void
    {
        self::assertSame(sprintf('%s:-1|c', $this->metricName), $this->readSocket());
    }

    protected function assertDecrementMetricWithNamespaceWasPushed(): void
    {
        self::assertSame(sprintf('%s.%s:-1|c', $this->namespace, $this->metricName), $this->readSocket());
    }

    protected function assertDecrementMetricWithTagsWasPushed(): void
    {
        self::assertSame(
            $this->withTags(sprintf('%s:-1|c', $this->metricName), $this->metricTags),
            $this->readSocket()
        );
    }

    protected function assertGaugeMetricWasPushed(): void
    {
        self::assertSame(sprintf('%s:%s|g', $this->metricName, $this->metricValue), $this->readSocket());
    }

    protected function assertGaugeMetricWithNamespaceWasPushed(): void
    {
        self::assertSame(
            sprintf('%s.%s:%s|g', $this->namespace, $this->metricName, $this->metricValue),
            $this->readSocket()
        );
    }

    protected function assertGaugeMetricWithTagsWasPushed(): void
    {
        self::assertSame(
            $this->withTags(sprintf('%s:%s|g', $this->metricName, $this->metricValue), $this->metricTags),
            $this->readSocket()
        );
    }

    protected function assertTimingMetricWasPushed(): void
    {
        self::assertSame(sprintf('%s:%s|ms', $this->metricName, $this->metricValue), $this->readSocket());
    }

    protected function assertTimingMetricWithNamespaceWasPushed(): void
    {
        self::assertSame(
            sprintf('%s.%s:%s|ms', $this->namespace, $this->metricName, $this->metricValue),
            $this->readSocket()
        );
    }

    protected function assertTimingMetricWithTagsWasPushed(): void
    {
        self::assertSame(
            $this->withTags(sprintf('%s:%s|ms', $this->metricName, $this->metricValue), $this->metricTags),
            $this->readSocket()
        );
    }

    protected function assertNoMetricsWerePushed(): void
    {
        self::assertEmpty($this->readSocket());
    }

    protected function withTags(string $input, array $tags): string
    {
        if (empty($tags)) {
            return $input;
        }

        $input .= '|#';
        foreach ($tags as $key => $value) {
            $input .= sprintf('%s:%s,', $key, $value);
        }

        return rtrim($input, ',');
    }
}
