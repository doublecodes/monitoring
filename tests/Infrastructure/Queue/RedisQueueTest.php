<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Queue;

use PHPUnit\Framework\TestCase;
use Predis\ClientInterface;
use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Infrastructure\Queue\RedisQueue;
use SplQueue;

class RedisQueueTest extends TestCase
{
    private RedisQueue $queue;

    protected function setUp(): void
    {
        parent::setUp();

        $stack = new SplQueue();
        $redisClient = $this->createMock(ClientInterface::class);
        $redisClient->method('__call')->willReturnCallback(function ($method, $args) use ($stack) {
            switch ($method) {
                case 'rpush':
                    $stack->push($args[1][0]);
                    break;
                case 'lpop':
                    return $stack->isEmpty() ? null : $stack->shift();
            }
        });
        $this->queue = new RedisQueue($redisClient);
    }

    public function testQueue()
    {
        //these are not separate tests because the set up takes a long time

        $this->assertIncrementMetricCanBeQueued();
        $this->assertDecrementMetricCanBeQueued();
        $this->assertGaugeMetricCanBeQueued();
        $this->assertTimingMetricCanBeQueued();
    }

    private function assertIncrementMetricCanBeQueued()
    {
        $metric = Metrics::incrementMetric('test.increment');
        $this->queue->push($metric);
        /** @var IncrementMetric $queuedMetric */
        $queuedMetric = $this->queue->pop();
        self::assertInstanceOf(IncrementMetric::class, $queuedMetric);
        self::assertSame($metric->getName(), $queuedMetric->getName());
        self::assertNull($this->queue->pop());
    }

    private function assertDecrementMetricCanBeQueued()
    {
        $metric = Metrics::decrementMetric('test.decrement');
        $this->queue->push($metric);
        /** @var DecrementMetric $queuedMetric */
        $queuedMetric = $this->queue->pop();
        self::assertInstanceOf(DecrementMetric::class, $queuedMetric);
        self::assertSame($metric->getName(), $queuedMetric->getName());
        self::assertNull($this->queue->pop());
    }

    private function assertGaugeMetricCanBeQueued()
    {
        $metric = Metrics::gaugeMetric('test.gauge', 1.0);
        $this->queue->push($metric);
        /** @var GaugeMetric $queuedMetric */
        $queuedMetric = $this->queue->pop();
        self::assertInstanceOf(GaugeMetric::class, $queuedMetric);
        self::assertSame($metric->getName(), $queuedMetric->getName());
        self::assertSame($metric->getValue(), $queuedMetric->getValue());
        self::assertNull($this->queue->pop());
    }

    private function assertTimingMetricCanBeQueued()
    {
        $metric = Metrics::timingMetric('test.timing', 1.0);
        $this->queue->push($metric);
        /** @var TimingMetric $queuedMetric */
        $queuedMetric = $this->queue->pop();
        self::assertInstanceOf(TimingMetric::class, $queuedMetric);
        self::assertSame($metric->getName(), $queuedMetric->getName());
        self::assertSame($metric->getTiming(), $queuedMetric->getTiming());
        self::assertNull($this->queue->pop());
    }
}
