<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Queue;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Infrastructure\Queue\InMemoryQueue;

class InMemoryQueueTest extends TestCase
{
    public function testQueue()
    {
        $queue = new InMemoryQueue();
        $metric = Metrics::incrementMetric('test.metric');
        $queue->push($metric);
        $queuedMetric = $queue->pop();
        self::assertSame($metric, $queuedMetric);
        self::assertNull($queue->pop());
    }
}
