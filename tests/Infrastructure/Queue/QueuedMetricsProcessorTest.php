<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Queue;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Monitor\Monitor;
use Talentry\Monitoring\Infrastructure\Monitor\QueuedMonitor;
use Talentry\Monitoring\Infrastructure\Queue\InMemoryQueue;
use Talentry\Monitoring\Infrastructure\Queue\QueuedMetricsProcessor;

class QueuedMetricsProcessorTest extends TestCase
{
    public function testRun()
    {
        $queue = new InMemoryQueue();
        $monitor = new QueuedMonitor($this->createMock(Monitor::class), $queue);
        $processor = new QueuedMetricsProcessor($monitor);

        $queue->push(Metrics::incrementMetric('foo'));
        $queue->push(Metrics::incrementMetric('bar'));

        $count = $processor->run();
        self::assertSame(2, $count);
    }
}
