<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Monitor;

use Talentry\Monitoring\Domain\Metric\UnsupportedMetricException;
use Talentry\Monitoring\Domain\Monitor\DecrementMonitor;
use Talentry\Monitoring\Domain\Monitor\GaugeMonitor;
use Talentry\Monitoring\Domain\Monitor\IncrementMonitor;
use Talentry\Monitoring\Domain\Monitor\TimingMonitor;
use Talentry\Monitoring\Infrastructure\Monitor\CompositeMonitor;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Tests\Mock\MetricsWithTags\DecrementMetricWithTags;
use Talentry\Monitoring\Tests\Mock\MetricsWithTags\GaugeMetricWithTags;
use Talentry\Monitoring\Tests\Mock\MetricsWithTags\IncrementMetricWithTags;
use Talentry\Monitoring\Tests\Mock\MetricsWithTags\TimingMetricWithTags;

class CompositeMonitorTest extends TestCase
{
    private CompositeMonitor $monitor;
    private MetricStore $metricStore;
    private IncrementMetric $incrementMetric;
    private DecrementMetric $decrementMetric;
    private GaugeMetric $gaugeMetric;
    private TimingMetric $timingMetric;
    private object $unsupportedMetric;
    private float $value;
    private string $namespace = 'ns';

    protected function setUp(): void
    {
        parent::setUp();

        $this->value = 42.0;
        $this->metricStore = $this->createMock(MetricStore::class);
        $this->monitor = new CompositeMonitor();
        $this->monitor->addMonitor(new DecrementMonitor($this->metricStore, $this->namespace));
        $this->monitor->addMonitor(new IncrementMonitor($this->metricStore, $this->namespace));
        $this->monitor->addMonitor(new GaugeMonitor($this->metricStore, $this->namespace));
        $this->monitor->addMonitor(new TimingMonitor($this->metricStore, $this->namespace));

        $this->decrementMetric = Metrics::decrementMetric('decrement');
        $this->incrementMetric = Metrics::incrementMetric('increment');
        $this->gaugeMetric = Metrics::gaugeMetric('gauge', $this->value);
        $this->timingMetric = Metrics::timingMetric('timing', $this->value);
        $this->unsupportedMetric = new class implements Metric {
            public function getName(): string
            {
                return 'test.unsupported';
            }
        };
    }

    public function testSupports()
    {
        self::assertTrue($this->monitor->supports($this->incrementMetric));
        self::assertTrue($this->monitor->supports($this->decrementMetric));
        self::assertTrue($this->monitor->supports($this->gaugeMetric));
        self::assertTrue($this->monitor->supports($this->timingMetric));
        self::assertFalse($this->monitor->supports($this->unsupportedMetric));
    }

    public function testPushIncrementMetric()
    {
        $this
            ->metricStore
            ->expects(self::once())
            ->method('increment')
            ->with('increment', 1.0, [], $this->namespace)
        ;

        $this->monitor->push($this->incrementMetric);
    }

    public function testPushIncrementMetricWithTags()
    {
        $tags = ['foo' => 'bar'];
        $name = 'increment';

        $this
            ->metricStore
            ->expects(self::once())
            ->method('increment')
            ->with($name, 1.0, $tags, $this->namespace)
        ;

        $this->monitor->push(new IncrementMetricWithTags($name, $tags));
    }

    public function testPushDecrementMetric()
    {
        $this
            ->metricStore
            ->expects(self::once())
            ->method('decrement')
            ->with('decrement', 1.0, [], $this->namespace)
        ;

        $this->monitor->push($this->decrementMetric);
    }

    public function testPushDecrementMetricWithTags()
    {
        $tags = ['foo' => 'bar'];
        $name = 'decrement';

        $this
            ->metricStore
            ->expects(self::once())
            ->method('decrement')
            ->with($name, 1.0, $tags, $this->namespace)
        ;

        $this->monitor->push(new DecrementMetricWithTags($name, $tags));
    }

    public function testPushGaugeMetric()
    {
        $this
            ->metricStore
            ->expects(self::once())
            ->method('gauge')
            ->with('gauge', $this->value, 1.0, [], $this->namespace)
        ;

        $this->monitor->push($this->gaugeMetric);
    }

    public function testPushGaugeMetricWithTags()
    {
        $tags = ['foo' => 'bar'];
        $name = 'gauge';

        $this
            ->metricStore
            ->expects(self::once())
            ->method('gauge')
            ->with($name, $this->value, 1.0, $tags, $this->namespace)
        ;

        $this->monitor->push(new GaugeMetricWithTags($name, $tags, $this->value));
    }

    public function testPushTimingMetric()
    {
        $this
            ->metricStore
            ->expects(self::once())
            ->method('timing')
            ->with('timing', $this->value, 1.0, [], $this->namespace)
        ;

        $this->monitor->push($this->timingMetric);
    }

    public function testPushTimingMetricWithTags()
    {
        $tags = ['foo' => 'bar'];
        $name = 'timing';

        $this
            ->metricStore
            ->expects(self::once())
            ->method('timing')
            ->with($name, $this->value, 1.0, $tags, $this->namespace)
        ;

        $this->monitor->push(new TimingMetricWithTags($name, $tags, $this->value));
    }

    public function testPushUnsupportedMetric()
    {
        $this->expectException(UnsupportedMetricException::class);

        $this->monitor->push($this->unsupportedMetric);
    }
}
