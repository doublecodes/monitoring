<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Monitor;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Infrastructure\Queue\InMemoryQueue;
use Talentry\Monitoring\Infrastructure\Monitor\QueuedMonitor;
use Talentry\Monitoring\Tests\Mock\MonitorMock;

class QueuedMonitorTest extends TestCase
{
    private MonitorMock $monitorMock;
    private QueuedMonitor $queuedMonitor;
    private InMemoryQueue $queue;
    private IncrementMetric $metric;

    protected function setUp(): void
    {
        parent::setUp();

        $this->queue = new InMemoryQueue();
        $this->monitorMock = new MonitorMock();
        $this->queuedMonitor = new QueuedMonitor($this->monitorMock, $this->queue);
        $this->metric = Metrics::incrementMetric('test.metric');
    }

    public function testSupports()
    {
        self::assertTrue($this->queuedMonitor->supports($this->metric));
    }

    public function testPush()
    {
        $this->queuedMonitor->push($this->metric);
        $queuedMetric = $this->queue->pop();
        self::assertSame($this->metric, $queuedMetric);
    }

    public function testProcessQueue()
    {
        $metric1 = Metrics::incrementMetric('test.metric1');
        $metric2 = Metrics::incrementMetric('test.metric2');
        $this->queuedMonitor->push($metric1);
        $this->queuedMonitor->push($metric2);

        $processed = $this->queuedMonitor->processQueue();
        self::assertSame(2, $processed);

        $pushedMetrics = $this->monitorMock->getPushedMetrics();
        self::assertSame($metric1, $pushedMetrics[0]);
        self::assertSame($metric2, $pushedMetrics[1]);
    }
}
