<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Monitor;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Domain\Metric\MetricStore;
use Talentry\Monitoring\Infrastructure\Monitor\MonitorFactory;
use Talentry\Monitoring\Infrastructure\Queue\Queue;

class MonitorFactoryTest extends TestCase
{
    private MetricStore $metricStore;
    private Queue $queue;
    private float $sampleRate = 1.0;
    private float $value = 2.0;

    protected function setUp(): void
    {
        parent::setUp();

        $this->metricStore = $this->createMock(MetricStore::class);
        $this->queue = $this->createMock(Queue::class);
    }

    public function testWithoutQueue(): void
    {
        $this->metricStore->expects(self::once())->method('increment')->with('increment');
        $this->metricStore->expects(self::once())->method('decrement')->with('decrement');
        $this->metricStore->expects(self::once())->method('gauge')->with('gauge', $this->value);
        $this->metricStore->expects(self::once())->method('timing')->with('timing', $this->value);

        $monitor = (new MonitorFactory($this->metricStore))->generate();
        $monitor->push(Metrics::incrementMetric('increment'));
        $monitor->push(Metrics::decrementMetric('decrement'));
        $monitor->push(Metrics::gaugeMetric('gauge', $this->value));
        $monitor->push(Metrics::timingMetric('timing', $this->value));
    }

    public function testWithQueue(): void
    {
        $metric = Metrics::incrementMetric('increment');
        $this->queue->expects(self::once())->method('push')->with($metric);

        $monitor = (new MonitorFactory($this->metricStore, $this->queue))->generate();
        $monitor->push($metric);
    }

    public function testWithoutMetricStore(): void
    {
        $this->metricStore->expects(self::never())->method('increment');
        $this->metricStore->expects(self::never())->method('decrement');
        $this->metricStore->expects(self::never())->method('gauge');
        $this->metricStore->expects(self::never())->method('timing');

        $factory = new MonitorFactory();
        $monitor = $factory->generate();
        $monitor->push(Metrics::incrementMetric('increment'));
        $monitor->push(Metrics::decrementMetric('decrement'));
        $monitor->push(Metrics::gaugeMetric('gauge', $this->value));
        $monitor->push(Metrics::timingMetric('timing', $this->value));
    }

    public function testDisabling(): void
    {
        $this->metricStore->expects(self::never())->method('increment');
        $this->metricStore->expects(self::never())->method('decrement');
        $this->metricStore->expects(self::never())->method('gauge');
        $this->metricStore->expects(self::never())->method('timing');

        $factory = new MonitorFactory($this->metricStore, null, false);
        $monitor = $factory->generate();
        $monitor->push(Metrics::incrementMetric('increment'));
        $monitor->push(Metrics::decrementMetric('decrement'));
        $monitor->push(Metrics::gaugeMetric('gauge', $this->value));
        $monitor->push(Metrics::timingMetric('timing', $this->value));
    }

    public function testWithNamespace(): void
    {
        $namespace = 'ns';
        $factory = new MonitorFactory($this->metricStore, null, true, $namespace);
        $monitor = $factory->generate();

        $this->metricStore
            ->expects(self::once())
            ->method('increment')
            ->with('increment', $this->sampleRate, [], $namespace);
        $this->metricStore
            ->expects(self::once())
            ->method('decrement')
            ->with('decrement', $this->sampleRate, [], $namespace);
        $this->metricStore
            ->expects(self::once())
            ->method('gauge')
            ->with('gauge', $this->value, $this->sampleRate, [], $namespace);
        $this->metricStore
            ->expects(self::once())
            ->method('timing')
            ->with('timing', $this->value, $this->sampleRate, [], $namespace);

        $monitor->push(Metrics::incrementMetric('increment'));
        $monitor->push(Metrics::decrementMetric('decrement'));
        $monitor->push(Metrics::gaugeMetric('gauge', $this->value));
        $monitor->push(Metrics::timingMetric('timing', $this->value));
    }
}
