<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Metric;

use Psr\Log\NullLogger;
use Psr\Log\Test\TestLogger;
use Talentry\Monitoring\Infrastructure\Metric\DatadogStatsdClient;
use Talentry\Monitoring\Tests\StatsdClientTest;

/**
 * @group integration
 */
class DatadogStatsdClientTest extends StatsdClientTest
{
    protected DatadogStatsdClient $client;

    protected function setUp(): void
    {
        parent::setUp();

        $logger = new NullLogger();
        $this->client = new DatadogStatsdClient($this->host, $this->port, $this->errorHandler, $logger);
    }

    public function testIncrement(): void
    {
        $this->client->increment($this->metricName);
        $this->assertIncrementMetricWasPushed();
    }

    public function testIncrementWithTags(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertIncrementMetricWithTagsWasPushed();
    }

    public function testIncrementWithNamespace(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, [], $this->namespace);
        $this->assertIncrementMetricWithNamespaceWasPushed();
    }

    public function testDecrement(): void
    {
        $this->client->decrement($this->metricName);
        $this->assertDecrementMetricWasPushed();
    }

    public function testDecrementWithTags(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertDecrementMetricWithTagsWasPushed();
    }

    public function testDecrementWithNamespace(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, [], $this->namespace);
        $this->assertDecrementMetricWithNamespaceWasPushed();
    }

    public function testGauge(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue);
        $this->assertGaugeMetricWasPushed();
    }

    public function testGaugeWithTags(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertGaugeMetricWithTagsWasPushed();
    }

    public function testGaugeWithNamespace(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertGaugeMetricWithNamespaceWasPushed();
    }

    public function testTiming(): void
    {
        $this->client->timing($this->metricName, $this->metricValue);
        $this->assertTimingMetricWasPushed();
    }

    public function testTimingWithTags(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertTimingMetricWithTagsWasPushed();
    }

    public function testTimingWithNamespace(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertTimingMetricWithNamespaceWasPushed();
    }

    public function testInvalidHost(): void
    {
        $logger = new TestLogger();
        $client = new DatadogStatsdClient('no-such-host', $this->port, $this->errorHandler, $logger);

        $client->increment('test');

        self::assertTrue($logger->hasErrorThatContains('socket_sendto()'));
    }

    protected function withTags(string $input, array $tags): string
    {
        if (empty($tags)) {
            return $input;
        }

        $input .= '|';
        foreach ($tags as $key => $value) {
            $input .= sprintf('#%s:%s,', $key, $value);
        }

        return rtrim($input, ',');
    }
}
