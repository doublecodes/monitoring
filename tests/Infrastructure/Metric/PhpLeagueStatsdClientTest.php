<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Metric;

use Psr\Log\Test\TestLogger;
use Talentry\Monitoring\Infrastructure\Metric\PhpLeagueStatsdClient;
use Talentry\Monitoring\Tests\StatsdClientTest;

class PhpLeagueStatsdClientTest extends StatsdClientTest
{
    private PhpLeagueStatsdClient $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->client = new PhpLeagueStatsdClient($this->host, $this->port);
    }

    public function testIncrement(): void
    {
        $this->client->increment($this->metricName);
        $this->assertIncrementMetricWasPushed();
    }

    public function testIncrementWithTags(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertIncrementMetricWithTagsWasPushed();
    }

    public function testIncrementWithNamespace(): void
    {
        $this->client->increment($this->metricName, $this->sampleRate, [], $this->namespace);
        $this->assertIncrementMetricWithNamespaceWasPushed();
    }

    public function testDecrement(): void
    {
        $this->client->decrement($this->metricName);
        $this->assertDecrementMetricWasPushed();
    }

    public function testDecrementWithTags(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, $this->metricTags);
        $this->assertDecrementMetricWithTagsWasPushed();
    }

    public function testDecrementWithNamespace(): void
    {
        $this->client->decrement($this->metricName, $this->sampleRate, [], $this->namespace);
        $this->assertDecrementMetricWithNamespaceWasPushed();
    }

    public function testGauge(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue);
        $this->assertGaugeMetricWasPushed();
    }

    public function testGaugeWithTags(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertGaugeMetricWithTagsWasPushed();
    }

    public function testGaugeWithNamespace(): void
    {
        $this->client->gauge($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertGaugeMetricWithNamespaceWasPushed();
    }

    public function testTiming(): void
    {
        $this->client->timing($this->metricName, $this->metricValue);
        $this->assertTimingMetricWasPushed();
    }

    public function testTimingWithTags(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, $this->metricTags);
        $this->assertTimingMetricWithTagsWasPushed();
    }

    public function testTimingWithNamespace(): void
    {
        $this->client->timing($this->metricName, $this->metricValue, $this->sampleRate, [], $this->namespace);
        $this->assertTimingMetricWithNamespaceWasPushed();
    }

    public function testInvalidHost(): void
    {
        $logger = new TestLogger();
        $client = new PhpLeagueStatsdClient('no-such-host', $this->port, $logger);
        $client->increment('test');
        self::assertTrue($logger->hasErrorThatContains('Name or service not known'));
    }
}
