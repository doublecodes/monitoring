<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Serializer;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Infrastructure\Serializer\SerializerFactory;
use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\GaugeMetric;
use Talentry\Monitoring\Domain\Metric\Model\IncrementMetric;
use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;
use Talentry\Monitoring\Domain\Metric\Metrics;
use Talentry\Monitoring\Infrastructure\Serializer\Serializer;

class CompositeSerializerTest extends TestCase
{
    private Serializer $serializer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->serializer = (new SerializerFactory())->generate();
    }

    public function testSerializeAndUnserializeGaugeMetric()
    {
        $metric = Metrics::gaugeMetric('test.gauge', 1.0);
        $serialized = $this->serializer->serialize($metric);

        /** @var GaugeMetric $unserialized */
        $unserialized = $this->serializer->unserialize($serialized);
        self::assertInstanceOf(GaugeMetric::class, $unserialized);
        self::assertSame('test.gauge', $unserialized->getName());
        self::assertSame(1.0, $unserialized->getValue());
    }

    public function testSerializeAndUnserializeDecrementMetric()
    {
        $metric = Metrics::decrementMetric('test.decrement');
        $serialized = $this->serializer->serialize($metric);

        /** @var DecrementMetric $unserialized */
        $unserialized = $this->serializer->unserialize($serialized);
        self::assertInstanceOf(DecrementMetric::class, $unserialized);
        self::assertSame('test.decrement', $unserialized->getName());
    }

    public function testSerializeAndUnserializeIncrementMetric()
    {
        $metric = Metrics::incrementMetric('test.increment');
        $serialized = $this->serializer->serialize($metric);

        /** @var IncrementMetric $unserialized */
        $unserialized = $this->serializer->unserialize($serialized);
        self::assertInstanceOf(IncrementMetric::class, $unserialized);
        self::assertSame('test.increment', $unserialized->getName());
    }

    public function testSerializeAndUnserializeTimingMetric()
    {
        $metric = Metrics::timingMetric('test.timing', 1.0);
        $serialized = $this->serializer->serialize($metric);

        /** @var TimingMetric $unserialized */
        $unserialized = $this->serializer->unserialize($serialized);
        self::assertInstanceOf(TimingMetric::class, $unserialized);
        self::assertSame('test.timing', $unserialized->getName());
        self::assertSame(1.0, $unserialized->getTiming());
    }
}
