<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Infrastructure\Serializer;

use PHPUnit\Framework\TestCase;
use Talentry\Monitoring\Infrastructure\Monitor\CompositeMonitor;
use Talentry\Monitoring\Infrastructure\Serializer\SerializerFactory;
use Talentry\Monitoring\Infrastructure\Serializer\CompositeSerializer;

class SerializerFactoryTest extends TestCase
{
    public function testGenerate()
    {
        $factory = new SerializerFactory();

        /** @var CompositeMonitor $simulated */
        $serializer = $factory->generate();

        self::assertInstanceOf(CompositeSerializer::class, $serializer);
    }
}
