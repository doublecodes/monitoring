<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Mock;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Monitor\Monitor;

class MonitorMock implements Monitor
{
    /**
     * @var Metric[]
     */
    private array $pushed;

    public function push(Metric $metric): void
    {
        $this->pushed[] = $metric;
    }

    public function supports(Metric $metric): bool
    {
        return true;
    }

    /**
     * @return Metric[]
     */
    public function getPushedMetrics(): array
    {
        return $this->pushed;
    }
}
