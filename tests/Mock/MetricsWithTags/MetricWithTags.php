<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Mock\MetricsWithTags;

use Talentry\Monitoring\Domain\Metric\Model\Metric;
use Talentry\Monitoring\Domain\Tags\HasTags;

class MetricWithTags implements Metric, HasTags
{
    private const DEFAULT_VALUE = 0.0;

    public function __construct(
        private string $name,
        private array $tags,
        private float $value = self::DEFAULT_VALUE,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getValue(): float
    {
        return $this->value;
    }
}
