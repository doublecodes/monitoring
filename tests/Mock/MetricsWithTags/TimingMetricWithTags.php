<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Mock\MetricsWithTags;

use Talentry\Monitoring\Domain\Metric\Model\TimingMetric;

class TimingMetricWithTags extends MetricWithTags implements TimingMetric
{
    public function getTiming(): float
    {
        return $this->getValue();
    }
}
