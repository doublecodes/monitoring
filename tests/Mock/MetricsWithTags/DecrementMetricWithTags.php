<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Mock\MetricsWithTags;

use Talentry\Monitoring\Domain\Metric\Model\DecrementMetric;

class DecrementMetricWithTags extends MetricWithTags implements DecrementMetric
{
}
