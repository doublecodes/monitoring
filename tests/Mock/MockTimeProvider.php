<?php

declare(strict_types=1);

namespace Talentry\Monitoring\Tests\Mock;

use Talentry\Monitoring\Infrastructure\Time\TimeProvider;

class MockTimeProvider extends TimeProvider
{
    private int $timestamp;

    public function __construct(?int $timestamp = null)
    {
        $this->timestamp = $timestamp ?? (new TimeProvider())->getCurrentTimeStampInMilliseconds();
    }

    public function getCurrentTimeStampInMilliseconds(): int
    {
        return $this->timestamp;
    }
}
