# Monitoring library

This library provides the means for pushing metrics to a monitoring service.
Currently, it supports pushing metrics to any StatsD compliant implementation, including Datadog.

# Types of metrics

### [Increment metric](src/Domain/Metric/Model/IncrementMetric.php)

Useful for tracking data that accumulates over time, for example tracking the number of requests.
This is a simple metric that is only identified by name. Every time you push this metric, it is incremented by 1.

### [Decrement metric](src/Domain/Metric/Model/DecrementMetric.php)

Same as increment metric, but inverse (decrements rather than increments).

### [Gauge metric](src/Domain/Metric/Model/GaugeMetric.php)

Useful for tracking the state that changes over time, for example the server load.
It is identified by both name and value.

### [Timing metric](src/Domain/Metric/Model/TimingMetric.php)

Similar to gauge metric, it is identified by name and value (except that in this case the value is a timing measure).
Useful for tracking timings, like response time. Time is given in milliseconds.

# How to use

- Implement one of the four metric types (specified above).
Alternatively you can use the [Metrics utility class](src/Domain/Metric/Metrics.php) to create metrics on the fly.
Implementing your own metric is preferred.
- Create the [Monitor](src/Domain/Monitor/Monitor.php) instance using the [MonitorFactory](src/Infrastructure/Monitor/MonitorFactory.php)
- Create an instance of your metric and push it using the Monitor (using `push` method).

# Naming metrics

Use dot-separated notation for creating namespaces, such as:

- `system.load.memory`
- `system.load.disk`

# Examples

## Create a Monitor for StatsD metrics

```
use Talentry\Monitoring\Infrastructure\Metric\PhpLeagueStatsdClient;
use Talentry\Monitoring\Infrastructure\Monitor\MonitorFactory;
use Talentry\Monitoring\Infrastructure\Metric\MetricStoreFactory;

$metricStore = (new MetricStoreFactory('statsd-host', 8125))->generate();
$monitor = (new MonitorFactory($metricStore))->generate();
```
